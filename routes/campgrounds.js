
var express=require("express");
var router= express.Router();
var Campground =require("../models/campground");
var Comment=require("../models/comment");
var middleware=require("../middleware");
router.get("/", function(req, res){
    var noMatch = null;
    if(req.query.search) {
        const regex = new RegExp(escapeRegex(req.query.search), 'gi');
        // Get all campgrounds from DB
        Campground.find({name: regex}, function(err, allCampgrounds){
           if(err){
               console.log(err);
           } else {
              if(allCampgrounds.length < 1) {
                  noMatch = "No campgrounds match that query, please try again.";
              }
              res.render("campgrounds/index",{campgrounds:allCampgrounds, noMatch: noMatch});
           }
        });
    } else {
        // Get all campgrounds from DB
        Campground.find({}, function(err, allCampgrounds){
           if(err){
               console.log(err);
           } else {
              res.render("campgrounds/index",{campgrounds:allCampgrounds, noMatch: noMatch});
           }
        });
    }
});


router.get("/new",middleware.isloggedin,function(req,res){
	res.render("campgrounds/new");
});

router.post("/",middleware.isloggedin,function(req,res){
	var name=req.body.name;
	var img=req.body.img;
	var desc=req.body.desc;
	var cost = req.body.cost;
	var author={
		id:req.user._id,
		username:req.user.username
	};
	var newcamp={name:name,cost: cost,img:img,desc:desc,author:author};
	Campground.create(newcamp,function(err,newly){
		if(err)
			console.log(err);
		else{
			console.log(newly);
					req.flash("success","Campground Successfully Added!!");
			res.redirect("/campgrounds");
		}
	});
});


router.get("/:id",function(req,res){
	Campground.findById(req.params.id).populate("comments").exec(function(err,foundCampground){
		if(err){ 
			console.log(err);
		}else{
			console.log(foundCampground);
			res.render("campgrounds/show",{campground:foundCampground});
		}
	});
});


//update campground

router.get("/:id/edit",middleware.check, function(req,res){
	Campground.findById(req.params.id,function(err,foundCampground){
	res.render("campgrounds/edit",{campground:foundCampground});
});

});

router.put("/:id",middleware.check ,function(req,res){
	Campground.findByIdAndUpdate(req.params.id ,req.body.campground, function(err,updatedcampground){
		if(err){res.redirect("/campgrounds");
					req.flash("error","Campground not found");
		
	}
		else
			res.redirect("/campgrounds/"+req.params.id);
	});

});


//delete campground
 
router.delete("/:id",middleware.check,function(req,res){
	Campground.findByIdAndRemove(req.params.id, function(err){
		res.redirect("/campgrounds");
	});
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};



module.exports=router;
//========================
//comments routes
//========================
