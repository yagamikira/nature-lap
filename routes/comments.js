var express=require("express");
var router= express.Router({mergeParams:true});
var Campground =require("../models/campground");
var Comment=require("../models/comment");
var middleware=require("../middleware");
router.get("/new",middleware.isloggedin,function(req,res){
	Campground.findById(req.params.id,function(err,campground){
		if(err)console.log(err);
		else
			res.render("comments/new",{campground:campground});
	})
	

});

router.post("/",middleware.isloggedin,function(req,res){
	Campground.findById(req.params.id,function(err,campground){
		if(err){console.log(err);
			res.redirect("/campgrounds");}
			
		else
		{

			 Comment.create(req.body.comment,function(err,comment){
				if(err){
					req.flash("error","Somethings Wrong");

					console.log(err);
				}
				else
				{
					comment.author.id=req.user._id;
					comment.author.username=req.user.username;
					var q=isstr(req);
					if(q)
					{
					comment.save();
					campground.comments.push(comment);
					campground.save();
					console.log(comment);
					req.flash("success","Successfully added comment");

					}
					res.redirect("/campgrounds/"+campground._id);
					

				}
			});
		}
	});
});
 
router.get("/:comment_id/edit",middleware.check2,function(req,res){
	Comment.findById(req.params.comment_id,function(err,foundcomment){
		if(err)res.redirect("back");
		else
		{
			res.render("comments/edit",{campgroundid:req.params.id,comment:foundcomment});

		}
	});
});

router.put("/:comment_id",middleware.check2,function(req,res){
	Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function(err,updatecomment){
		if(err){console.log(err);
			res.redirect("back");}
		else{
			res.redirect("/campgrounds/"+req.params.id);
		}
});
});

router.delete("/:comment_id",middleware.check2,function(req,res){
	Comment.findByIdAndDelete(req.params.comment_id,function(err){
		if(err)
			res.redirect("back");
		else{
					req.flash("success","Comment deleted");

		res.redirect("/campgrounds/"+req.params.id);}
	});
});


function isstr(req,res) {
	var str=req.body.comment.text;
	var n=str.length;
	if(n<=0)return 0;
	var f=0;
   for(var i=0;i<str.length;i++)
   {
   	  if((str[i]>='a'&&str[i]<='z')||(str[i]>='0'&&str[i]<='9')||(str[i]>='A'&&str[i]<='Z'))
   	  	{
   	  		f=1;
   	  		break;
   	  	}

   }
   return f;

}


module.exports=router;