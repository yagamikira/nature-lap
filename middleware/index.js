var Campground=require("../models/campground");
var Comment=require("../models/comment");


var middlewareobj={};


middlewareobj.check=function(req,res,next)
{
	if(req.isAuthenticated()){
			Campground.findById(req.params.id,function(err,foundCampground){
				if(err){
					req.flash("error","Campground not found");
				res.redirect("back");
				}
				else{
				if(foundCampground.author.id.equals(req.user._id)||(req.user.isAdmin))
					next();
				else{
					req.flash("error","You dont have permission");

					res.redirect("back");
				}

				}
		});
		}
		else
		{
			req.flash("error","Please Login First");
			res.redirect("back");
		}
};

middlewareobj.check2=function(req,res,next){
	if(req.isAuthenticated())
	{
		Comment.findById(req.params.comment_id,function(err,foundComment){
				if(err){
				res.redirect("back");
				}
				else{
				if(foundComment.author.id.equals(req.user._id)||(req.user.isAdmin))
					next();
				else{
					req.flash("error","Permission denied");

					res.redirect("back");
				}

				}
		});
	}
	else{
					req.flash("error","Please Login First");
		res.redirect("back");
	}
};

middlewareobj.isloggedin=function(req,res,next){
	if(req.isAuthenticated()){
		return next();
	}
	req.flash("error","Please Login First");
	res.redirect("/login");
};

module.exports=middlewareobj;