var express 			=require("express"), 
	app 				=express(), 
	bodyparser 			=require("body-parser"),
	mongoose 			=require("mongoose"),
	Campground 			=require("./models/campground"),
	seedDB 				=require("./seeds"),
	Comment 			=require("./models/comment"),
	passport 			=require("passport"),
	LocalStrategy 		=require("passport-local"),
	methodoverride 		=require("method-override"),
	passportLocalMongoose =require("passport-local-mongoose"),
	user 				=require("./models/users");
var	commentroutes 		=require("./routes/comments"),
	campgroundroutes 	=require("./routes/campgrounds"),
	flash 				=require("connect-flash"),
	indexroutes			=require("./routes/index");




   mongoose.connect(process.env.DATABASEURL,{ useNewUrlParser: true , useUnifiedTopology: true,
   useCreateIndex: true,
   useFindAndModify: false});
app.set("view engine","ejs");
app.use(bodyparser.urlencoded({extended:true}));
// seedDB();
app.use(express.static(__dirname+"/public"));
app.use(methodoverride("_method"));
app.use(require("express-session")({
	secret:"Haku Na Matata",
	 resave:false,
	 saveUninitialized:false
}));
app.use(flash());
app.locals.moment=require('moment');
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(user.authenticate()));
passport.serializeUser(user.serializeUser());
passport.deserializeUser(user.deserializeUser())
app.use(function(req,res,next){
	res.locals.error=req.flash("error");
	res.locals.success=req.flash("success");

	res.locals.curr=req.user;
	next();
});
app.use("/campgrounds",campgroundroutes);
app.use("/campgrounds/:id/comments",commentroutes);
app.use("/",indexroutes);


app.listen(process.env.PORT,process.env.IP,function(){
	console.log("NatureLap is Started");
});

